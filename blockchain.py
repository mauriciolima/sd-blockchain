# -*- coding: utf-8 -*-
from block import Block
import datetime
import hashlib
import random

num_zonas_eleitorais = 10

block_chain = [Block.create_genesis_block()]

print("The genesis block has been created.")
print("Hash: %s" % block_chain[0].hash)

for i in range(1, num_zonas_eleitorais):
    block_chain.append(Block(block_chain[i-1].hash,
                             {'maria': random.randint(0,100), 'jose': random.randint(0,100), 'joao': random.randint(0,100)},
                             datetime.datetime.now()))

    print("Zona Eleitoral #%d criada." % i)
    print("PrevHash: %s" % block_chain[-1].prev_hash)
    print("Hash: %s" % block_chain[-1].hash)
    print("Votos Maria: %s" % block_chain[-1].data['maria'])
    print("Votos Jose: %s" %  block_chain[-1].data['jose'])
    print("Votos Joao: %s" %  block_chain[-1].data['joao'])

qntVotosMaria = 0
qntVotosJose = 0
qntVotosJoao = 0
j = 0
for j in range(1, 9):
    qntVotosMaria = qntVotosMaria + block_chain[j].data['maria']
    qntVotosJose = qntVotosJose + block_chain[j].data['jose']
    qntVotosJoao = qntVotosJoao + block_chain[j].data['joao']
    j = j + 1

print("##### RESULTADOS ELEIÇÕES 2018 ####### \n")
print("Maria: %s \n" % qntVotosMaria)
print("Jose: %s \n" % qntVotosJose)
print("Joao: %s \n" % qntVotosJoao)


def calculaHash(prev_hash, data, timestamp):
    header = (str(
        prev_hash) +
              str(data) +
              str(timestamp))

    block_hash = hashlib.sha256(header.encode()).hexdigest().encode()

    return block_hash

def validaBlockchainTrue(blockchain):

    for j in range(1, 10):
        if(blockchain[j-1].hash == calculaHash(blockchain[j-1].prev_hash,blockchain[j-1].data,blockchain[j-1].timestamp)):
            a = True
        else:
            a = False
            break

    if(a):
        print("Blockchain válida")
    else:
        print("Blockchain inválida")

def validaBlockchainInvalida(blockchain, j):
    blockchain[j].hash = hashlib.sha256(str(blockchain[j].prev_hash) +str(
                                        {'maria': random.randint(0, 100), 'jose': random.randint(0, 100),
                                         'joao': random.randint(0, 100)}) + str(blockchain[j].timestamp)).hexdigest().encode()

    print("New Hash: %s" %blockchain[j].hash)

    validaBlockchainTrue(blockchain)


validaBlockchainTrue(block_chain)

print("###########################\n")
validaBlockchainInvalida(block_chain,6)