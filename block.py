import datetime
import hashlib

class Block:
    def __init__(self, prev_hash, data, timestamp):
        self.prev_hash = prev_hash
        self.data = data
        self.timestamp = timestamp
        self.hash = self.generateHash()


    def generateHash(self):
        header = (str(
            self.prev_hash) +
            str(self.data) +
            str(self.timestamp))

        block_hash = hashlib.sha256(header.encode()).hexdigest().encode()

        return block_hash

    @staticmethod
    def create_genesis_block():
        return Block("0", "0", datetime.datetime.now())